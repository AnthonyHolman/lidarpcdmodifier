**********************************
LidarPCDModifier readme

Anthony Holman, 2016
**********************************

---------------------
Program Overview
---------------------
This program implements various functions from the PointCloud library (pointclouds.org) in order 
to modify pointcloud files (.pcd) to better analyze data acquired from LIDAR.

The program is able to load and save .pcd files in ascii and binary format, filter points that fall
outside of a 2D container, and create histograms of the heights of the points. 

---------------------
Install Instructions
---------------------
Check the INSTALL file for details. 

---------------------
Function Overview
---------------------
**-- Load a new file**

This loads a .pcd file into the program. Currently the program only uses PointXYZ point types. Other
point types can be read into the program, but the file output will lose any additonal information
that the original file had other than the X, Y, and Z points.

While a .pcd file can be loaded from the program, another way to load the file into the program
is to pass in the full path of the .pcd file as an argument when running the program from the 
command line.

Linux Example:
./LidarPCDModifier /home/user/mypcdfile.pcd

Wildcards will work when loading as an argument, but not when loading the file from the program.

**-- Save the file**

This outputs a new .pcd file (or overwrites the old one if the directory and name is the same) 
containing all the changes you have made to the loaded .pcd file within the program. The function
offers to save your file in either ascii or binary. Binary has much better performance and will
make the load process much faster. However, ascii will make the file readable should you choose
to look at it with a text editor.

The function will ask you for the full path of where you would like to save the file:

Linux Example:
/home/user/newpcd.pcd

Windows Example:
C:\files\newpcd.pcd

**-- Remove all points within a specified 2D container**

This is a filtering process that removes points that don't fall within a X and Y container.

Example:

```
#!c++

                   p
(X4, Y4)-----------------(X3, Y3)
        |       p       |
        |               |  p
   p    |     p    p    |  
        | p             | p
        |  p     p      |
(X1, Y1)-----------------(X2, Y2)
             p
```
The p's that lie within the box would stay, but the p's outside would be removed.

The program offers two ways to filter. One that uses the PointCloud's crop hull function
and another that is a simple conditional statement. 

The crop hull function is much more flexible and has a lot more functionality, however, 
during testing there have been a couple of occasions where issues have occured with the 
function. The issue arises occasionaly when a user manually removes points from a .pcd 
file outside the program. When filtering after the removal, crop hull filters out all the 
points even if they lie outside the container. Therefore, for the best chance of success 
it's recommended to not edit the .pcd files in a text editor. 

The advantages of crop hull is that the user is able to create a container with as many
sides as the user pleases. The container can be skewed in any way. The points can be entered
in any order.

Crop Hull Container Example:
```

        (X,Y)----------(X,Y)
            /          |
           /           |
     (X,Y)/            /(X,Y)
          \           /
           \         /
            \       /
             \     /
              \   /
               \ /
               (X,Y)
```

Due to the mentioned issues, however. A simple conditional filter has been implemented as 
a last resort. The conditional filter can only filter outside a box with no skew.

Conditional Container Example:
```

    (Y2)-----------------
        |               | 
        |               |    
        |               |  
        |               |  
        |               |
(X1, Y1)-----------------(X2)        
```

The program offers to filter outside a single, user inputted container or from a file that
contains multiple verticies for multiple containers which creates .pcd files in batch. 

If the user selects to filter from a single container and crop hull is turned on, it will
prompt the user to enter in the amount of points that will make up the hull of the container.
If the program is set to conditional, the total points will automatically be set to 2.
Then the user can start inputting the coordinates that make up the container. The filtering
process will the begin and the loaded file will have the points filtered. No changes will
be made permanant until the user saves the loaded file.

If the the batch function is selected, the program will ask for the full path of the
information file that contains the verticies and if it has a header or not.

Example: C:\files\vertexfile.txt

The file should be in the format:
```
Header (optional)
FileMark, X1, Y1, X2, Y2, X3, Y3, X4,...
FileMark, X1, Y1, X2, Y2, X3, Y3, X4,...
FileMark, X1, Y1, X2, Y2, X3, Y3, X4,...
FileMark, X1, Y1, X2, Y2, X3, Y3, X4,...
...
```

Example:
```
Plot,UTM12E1,UTM12N1,UTM12E2,UTM12N2,UTM12E3,UTM12N3,UTM12E4,UTM12N4
101,   101589,     16088,   101620,     18081,   113809,     17919,   113779,     15933
102,   113809,     17919,   101620,     18081,   101651,     20123,   113841,     19972
103,   101682,     22170,   113872,     22013,   113841,     19972,   101651,     20123
104,   113872,     22013,   101682,     22170,   101713,     24148,   113902,     23986
```
Then the program will ask which directory will the files be saved in, followed by a descriptor
for the file. The format should look like: (descriptor)(_FileMark).pcd

So if a descriptor called test has been entered and it was ran with the example vertexfile.txt
above, the first file would be saved as: test_101.pcd

Finally, the program then asks if you want to save the files in binary or ascii as well 
as if you want to save the file with a plant height histogram .csv file (detailed in
the next section). Afterwards, the filtering process begins. Only pointclouds that have any
points in them will have a .pcd file generated. Empty ones are skipped over to prevent blanks
from being generated.

**-- Create plant height histogram**

This function generates a single .csv file containing a plant height range (bins), the total points
that have heights that fall within the range of the bins, and the cumulative total of points that
at each bin which will add up in the end to all the points in the .pcd file.

The function asks if you want to create the .csv with the same name as the loaded file and in the
same directory (won't ask for input after) or if you want to specify the directory and the name
of the .csv file.