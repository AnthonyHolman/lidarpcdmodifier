/* ==================================================================================
   Filename: pclXYFilter.cpp
   Author: Anthony Holman
   Date Created: April 28, 2016 
   Description: Filters out a pointcloud's points outside a specified container. See
                the header for general information about each function.
   ==================================================================================*/
#pragma once
/* ---------------------------------------------------------------------------------
   Include files
   ---------------------------------------------------------------------------------*/
#include "include/pclXYFilter.hpp"
// ---------------------------------------------------------------------------------

/* ---------------------------------------------------------------------------------
   Class: XYFilter
   ---------------------------------------------------------------------------------*/
XYFilter::XYFilter(){
	hull = pcl::PointCloud<pcl::PointXYZ>::Ptr (new pcl::PointCloud<pcl::PointXYZ>);
	createCSV = true;
	cropHullToggle = true;
}

// Function description 
int XYFilter::filterRoutine(){
	// Local variables.
	std::string inputHolder = " ";
	char userInput = ' ';

	// Checks to see if the file is empty to prevent crashing.
	if(fileOperations.getCloudPtr()->empty()){
		PCL_ERROR("Empty file. Returning to the menu...\n");
		return -1;
	}

	// Asking the user if they want to filter a single PCD file or create a bunch of filtered ones in a batch.
	// Also asks the user if they want to use crop hull or not.
	while(userInput == ' '){
		std::cout << "\n\nThis function will remove all point values outside a specified box.\n"
			 << "(1) Remove points manually from a single box (affects loaded file)\n"
			 << "(2) Remove points in batch from information file\n"
			 << "(3) Toggle crop hull (can use greater than 3 points, more accurate, might not work on all files)\n"
			 << "    vs. box(can't be skewed, can only pick x and y min and max) (crop hull =  " 
			 << (cropHullToggle == true ? "on" : "off") << ")\n"
			 << "(q) Return to the main menu\n"
		     << "Input: ";
		getline(std::cin,inputHolder);
		userInput = inputHolder[0];

		switch(userInput){
			case '1':
				return (setXYValues() == -1) ? -1 : 0; 
				break;
			case '2':
				return (prepareBatch() == -1) ? -1 : 0;
				break;
			case '3':
				cropHullToggle = (cropHullToggle == true ? false : true);
				std::cout << "Crop hull is now " << (cropHullToggle == true ? "on" : "off");
				userInput = ' ';
				break; 
			case 'q':
				std::cout << "Returning to menu";
				return -1;
				break;
			default:
				std::cout << "Invalid Input\n";
				userInput = ' ';
				break;
		}
	}
	userInput = ' ';
}

// TODO: Make the point entry for the non crop hull box less awkward and unclear.
// TODO: Error system to prevent the user from entering a non numerical value.
int XYFilter::setXYValues(){
	// Local variables
	std::string inputHolder = " ";
	char userInput = ' ';
	float x = 0, y = 0;
	int totalPoints = 0;
	bool validInput = false;

	// Asks the user for the amount of points they want to enter if they have the crop hull
	// option selected. If crop hull is turned off, put the total points to 2.
	if(cropHullToggle == true){		
		std::cout << "\nHow many points make up the vertices of your container? (minimum 3)\n";
		while(validInput == false){
			std::cout << "Input: ";
			getline(std::cin, inputHolder);
			try{
				totalPoints = stoi(inputHolder);
				validInput = true;
			}
			catch(std::invalid_argument&){
				std::cout << "Invalid Input\n";
				validInput = false;
			}
		}
		validInput = false; // Reset switch.

		if(totalPoints < 3){
			std::cout << "Invalid number of points, returning to the main menu...\n";
			return -1;
		}
	}
	else totalPoints = 2;

	// Creates a spot in the vector representing each index containing a point in the hull.
	for(int index = 0; index < totalPoints; index++){
		indices.vertices.push_back(index);
	}

	// The amount of points differs depending on how many points the user wants to enter.
	// Even though z isn't being used in the hull, for safety reasons it's being filled with
	// a value.
	std::cout << "Enter the X and Y coordinates of your 2D container.";
	for(int index = 0; index < totalPoints; index++){
		while(validInput == false){ // Setting x value.
			std::cout << "\nX" << index + 1 << ": ";
			getline(std::cin, inputHolder);
			try{
				x = stof(inputHolder);
				validInput = true;
			}
			catch(std::invalid_argument&){
				std::cout << "Invalid Input\n";
				validInput = false;
			}
		}
		validInput = false; // Reset switch.

		while(validInput == false){ // Setting y value.
			std::cout << "\nY" << index + 1 << ": ";
			getline(std::cin, inputHolder);
			try{
				y = stof(inputHolder);
				validInput = true;
			}
			catch(std::invalid_argument&){
				std::cout << "Invalid Input\n";
				validInput = false;
			}
		}
		validInput = false; // Reset switch

		hull->push_back(pcl::PointXYZ());
		hull->points[hull->size()-1].x = x;
		hull->points[hull->size()-1].y = y;
		hull->points[hull->size()-1].z = 0;
	}

	// Lists out each point for the user to review to check for accuracy.
	std::cout << "Inputted points:\n";
	for(int index = 0; index < hull->size(); index++){
		std::cout << "(" << hull->points[index].x << "," << hull->points[index].y << ")\n";
	}

	// Depending on how the user feels about their entries, they can return to the menu
	// or proceed with the filtering.
	std::cout << "Is this correct? (Enter 'Y' to continue or 'N' to return to the main menu) ";
	while(userInput == ' '){
		std::cout << "Input: ";
		getline(std::cin,inputHolder);
		userInput = inputHolder[0];

		switch(userInput){	
			case 'Y':
			case 'y':
				if(cropHullToggle == true){
					if(removeOutliersCropHull() == -1) return -1;
				}
				else{
					if(removeOutliers() == -1) return -1;
				}
				return 0;
				break;
			case 'N':
			case 'n':
				std::cout << "Returning to main menu.";
				hull->clear();
				indices.vertices.clear();
				return -1;
				break;
			default:
				std::cout << "Invalid Input\n";
				userInput = ' ';
				break;
		}
	}
}


int XYFilter::prepareBatch(){
	// Local variables.
	CanopyCalculations canopyClass;
	std::ifstream infoFile;
	std::string infoFilePath = "_", workingDir = "_", descriptor = "_", plot = "_", extension = ".pcd", fullPath = "_";
	std::string grabInfoFileLine = "_";
	std::string inputHolder = "_";
	std::string stringTest = "_";
	char userInput = ' ';
	int delimiter = 0;
	float x=0, y=0;
	bool hasHeader = true;
	bool saveFormat = true;

	// Simply asking if the information file has a header.
	std::cout << "\nThe information file needs to be in the following format: \n"
		 << "Plot ID, X1, Y1, X2, Y2, X3, Y3, X4, Y4...\n"
		 << "Does the information file have a header?\n"
		 << "(1) Yes\n"
		 << "(2) No\n"
		 << "(q) Return to main menu\n";

	while(userInput == ' '){
		std::cout << "Input: ";
		getline(std::cin,inputHolder);
		userInput = inputHolder[0];

		switch(userInput){
			case '1':
				hasHeader = true;
				break;
			case '2':
				hasHeader = false;
				break;
			case 'q':
				std::cout << "Returning to menu\n";
				return -1;
				break;
			default:
				std::cout << "Invalid Input\n\n";
				userInput = ' ';
				break;
		}
	}
	userInput = ' ';

	// Asking the user to enter the full path of the text file containing the verticies.
	// Then it loads it into the ifstream variable defined at the top of this function.
	std::cout << "\nEnter the full path (directory and file name) of the information file.\n"
		 << "Input: ";
	getline(std::cin,infoFilePath);
	infoFile.open(infoFilePath);
	if(!infoFile.is_open()){
		std::cout << "Could not find file!";
		return -1;
	}

	// Asking the user for the directory where they would like to save the files.
	std::cout << "\nEnter the working directory where you would like to save the PCD files.\n"
		 << "Input: ";
	getline(std::cin,workingDir);

	// Asking for the first part of the file name.
	std::cout << "\nThe format the files will be saved in is (descriptor)(_plot).pcd.\n"
		 << "Enter the descriptor portion of the file name (a date for example).\n"
		 << "Be sure to put in a trailing slash ('/' or '\\' depending on OS) at the end.\n"
		 << "Input: ";
	getline(std::cin,descriptor);

	// Checking with the user one last time if they inputted what they wanted.
	// Also checking to save it with histogram data.
	std::cout << "Save path will be: " << workingDir << descriptor << "_(plot).pcd\n"
		 << "(1) Save in ascii WITH canopy height histogram CSV\n"
		 << "(2) Save in ascii WITHOUT canopy height histogram CSV\n"
		 << "(3) Save in binary WITH canopy height histogram CSV\n"
		 << "(4) Save in binary WITHOUT canopy height histogram CSV\n"
		 << "(q) Return to main menu\n";


	while(userInput == ' '){
		std::cout << "Input: ";
		getline(std::cin,inputHolder);
		userInput = inputHolder[0];

		switch(userInput){
			case '1':
				saveFormat = true;
				createCSV = true;
				break;
			case '2':
				saveFormat = true;
				createCSV = false;
				break;
			case '3':
				saveFormat = false;
				createCSV = true;
				break;
			case '4':
				saveFormat = false;
				createCSV = false;
				break;
			case 'q':
				std::cout << "Returning to menu\n";
				return -1;
				break;
			default:
				std::cout << "Invalid Input\n\n";
				userInput = ' ';
				break;
		}
	}

	// Pulling out the header line before the process begins if the user says there is a header.
	if(hasHeader == true) getline(infoFile,grabInfoFileLine);

	// Will loop as long as there are lines in the file.
	while(getline(infoFile,grabInfoFileLine)){
		//Removing spaces
		grabInfoFileLine.erase(remove(
			grabInfoFileLine.begin(), grabInfoFileLine.end(), ' '), grabInfoFileLine.end());

		// Storing the first column of the file into the plot, and then every two columns after
		// into the x and y portion of the point cloud.
		plot = grabToken(grabInfoFileLine);
		while(!grabInfoFileLine.empty()){
			stringTest = grabToken(grabInfoFileLine);
			boost::algorithm::trim(stringTest); // This function cuts out the whitespace.
			x = stof(stringTest);
			stringTest = grabToken(grabInfoFileLine);
			boost::algorithm::trim(stringTest); 
			y = stof(stringTest);
			hull->push_back(pcl::PointXYZ());
			hull->points[hull->size()-1].x = x;
			hull->points[hull->size()-1].y = y;
			hull->points[hull->size()-1].z = 0; // Filling z value for safety purposes.
			indices.vertices.push_back(hull->size()-1); // Letting the vector know we have added another point.
		}

		// Concatinating all the various strings together that make up the full path for the filtered PCD file.
		fullPath = workingDir + descriptor + "_" + plot + extension;
		
		// Checking to see if we are using the crop hull functions or not.
		if(cropHullToggle == true){
			if(removeOutliersCropHull(fullPath, saveFormat, canopyClass) == -1) return -1;
		}
		else{
			if(removeOutliers(fullPath, saveFormat, canopyClass) == -1) return -1;
		}

		// Emptying the hull and the vectors used so they can be refilled on the next loop.
		hull->clear();
		indices.vertices.clear();
		if(createCSV == true) canopyClass.emptyBin();

	}
	// Since the filtering process is now done, the file is closed.
	infoFile.close();
	return 0;
}

std::string XYFilter::grabToken(std::string &fileLine){
	// Local variables.
	std::string token = " ";
	std::string delimiter = ",";
	int position = 0;

	// Finds the string up to the delimiter, assigns that portion to token, then deletes that part
	// of the string from fileLine. If there is no comma remaining, it assigns the remaining string
	// to token and deletes the rest of the string.
	position = fileLine.find(delimiter);
	token = fileLine.substr(0,position);
	if(position != -1){fileLine.erase(0, position + delimiter.length());}
	else fileLine.clear();

	return token;
}

// Uses cropHull. Easily superior if you can get it to work.
int XYFilter::removeOutliersCropHull(){
	// Local variables
	pcl::PointCloud<pcl::PointXYZ>::Ptr filteredCloud = pcl::PointCloud<pcl::PointXYZ>::Ptr (new pcl::PointCloud<pcl::PointXYZ>);
	pcl::CropHull<pcl::PointXYZ> removePoints;
	std::vector<pcl::Vertices> indicesVect;

	// Pushes the vector that is holding the index for the points in the hull into a 2D vector that's required for
	// crop hull. Unnessesary for what we are doing, but it won't work otherwise.
	indicesVect.push_back(indices);

	removePoints.setDim(2); // Telling crop hull that we only want to use the x and y coordinates in the cloud.
	removePoints.setInputCloud(fileOperations.getCloudPtr()); // Giving crop hull the cloud that is holding all the points.
	removePoints.setHullIndices(indicesVect); // Giving crop hull the 2D vector holding the verticies index.
	removePoints.setHullCloud(hull); // Giving crop hull the pointcloud that is holding the actual points for the verticies.
	removePoints.setCropOutside(true); // Telling crop hull that we want to filter all points outside of hull.
	removePoints.filter(*fileOperations.getCloudPtr()); // Replacing the points in the main cloud with the filtered ones.

	return 0;
}

int XYFilter::removeOutliersCropHull(std::string &filePath, bool &saveFormat, CanopyCalculations &canopyClass){
	pcl::PointCloud<pcl::PointXYZ>::Ptr filteredCloud = pcl::PointCloud<pcl::PointXYZ>::Ptr (new pcl::PointCloud<pcl::PointXYZ>);
	pcl::CropHull<pcl::PointXYZ> removePoints;
	std::vector<pcl::Vertices> indicesVect;

	// Pushes the vector that is holding the index for the points in the hull into a 2D vector that's required for
	// crop hull. Unnessesary for what we are doing, but it won't work otherwise.
	indicesVect.push_back(indices);

	removePoints.setDim(2); // Telling crop hull that we only want to use the x and y coordinates in the cloud.
	removePoints.setInputCloud(fileOperations.getCloudPtr()); // Giving crop hull the cloud that is holding all the points.
	removePoints.setHullIndices(indicesVect); // Giving crop hull the 2D vector holding the verticies index.
	removePoints.setHullCloud(hull); // Giving crop hull the pointcloud that is holding the actual points for the verticies.
	removePoints.setCropOutside(true); // Telling crop hull that we want to filter all points outside of hull.
	removePoints.filter(*filteredCloud); // Placing the filtered points in a new cloud.

	// Passing the cloud and the file path to the appropriate save function according to what the user asked for.
	// Also generates a histogram CSV if the user wanted it.
	std::cout << "Saving: " << filePath << std::endl;
	saveFormat == true ? fileOperations.saveAscii(filteredCloud, filePath) : fileOperations.saveBinary(filteredCloud, filePath);
	if(createCSV == true && !filteredCloud->empty()) canopyClass.canopyRoutine(filteredCloud,filePath);

	return 0;
}

int XYFilter::removeOutliers(){
	// Local variables.
	pcl::PointCloud<pcl::PointXYZ>::Ptr newCloud = pcl::PointCloud<pcl::PointXYZ>::Ptr (new pcl::PointCloud<pcl::PointXYZ>);
	float xMax, xMin, yMax, yMin;

	// Initializing the coordinates with the first point in the point cloud.
	xMax = hull->points[0].x;
	xMin = hull->points[0].x;
	yMax = hull->points[0].y;
	yMin = hull->points[0].y;

	// Goes through each additional point and checking if they are greater than the maxes or lower than the mins.
	for(int index = 1; index < hull->size();index++){
		if(hull->points[index].x > xMax){
			xMax = hull->points[index].x;
		}
		else if(hull->points[index].x < xMin){
			xMin = hull->points[index].x;
		}

		if(hull->points[index].y > yMax){
			yMax = hull->points[index].y;
		}
		else if(hull->points[index].y < yMin){
			yMin = hull->points[index].y;
		}
	}

	// Checking to see if the point in the main cloud is within the boundaries if the container.
	// If it is, it adds it to a new pointcloud.
	for(int index = 0; index < fileOperations.getCloudPtr()->size(); index++){		
		if(		   fileOperations.getCloudPtr()->points[index].x < xMax
				&& fileOperations.getCloudPtr()->points[index].x > xMin
				&& fileOperations.getCloudPtr()->points[index].y < yMax
				&& fileOperations.getCloudPtr()->points[index].y > yMin){

			newCloud->push_back(pcl::PointXYZ());
			newCloud->points[newCloud->size()-1].x = fileOperations.getCloudPtr()->points[index].x;
			newCloud->points[newCloud->size()-1].y = fileOperations.getCloudPtr()->points[index].y;
			newCloud->points[newCloud->size()-1].z = fileOperations.getCloudPtr()->points[index].z;
		}
	}

	// Swaps the main cloud's points with the filtered ones and then discards the old points when the function ends.
	fileOperations.getCloudPtr()->swap(*newCloud);

	return 0;
}

int XYFilter::removeOutliers(std::string &filePath, bool &saveFormat, CanopyCalculations &canopyClass){
	// Local variables.
	pcl::PointCloud<pcl::PointXYZ>::Ptr newCloud = pcl::PointCloud<pcl::PointXYZ>::Ptr (new pcl::PointCloud<pcl::PointXYZ>);
	float xMax, xMin, yMax, yMin;

	// Initializing the coordinates with the first point in the point cloud.
	xMax = hull->points[0].x;
	xMin = hull->points[0].x;
	yMax = hull->points[0].y;
	yMin = hull->points[0].y;

	// Goes through each additional point and checking if they are greater than the maxes or lower than the mins.
	for(int index = 1; index < hull->size();index++){
		if(hull->points[index].x > xMax){
			xMax = hull->points[index].x;
		}
		else if(hull->points[index].x < xMin){
			xMin = hull->points[index].x;
		}

		if(hull->points[index].y > yMax){
			yMax = hull->points[index].y;
		}
		else if(hull->points[index].y < yMin){
			yMin = hull->points[index].y;
		}
	}

	// Checking to see if the point in the main cloud is within the boundaries if the container.
	// If it is, it adds it to a new pointcloud.
	for(int index = 0; index < fileOperations.getCloudPtr()->size(); index++){
		if(		   fileOperations.getCloudPtr()->points[index].x < xMax
				&& fileOperations.getCloudPtr()->points[index].x > xMin
				&& fileOperations.getCloudPtr()->points[index].y < yMax
				&& fileOperations.getCloudPtr()->points[index].y > yMin){

			newCloud->push_back(pcl::PointXYZ());
			newCloud->points[newCloud->size()-1].x = fileOperations.getCloudPtr()->points[index].x;
			newCloud->points[newCloud->size()-1].y = fileOperations.getCloudPtr()->points[index].y;
			newCloud->points[newCloud->size()-1].z = fileOperations.getCloudPtr()->points[index].z;

		}
	}

	// Passing the cloud and the file path to the appropriate save function according to what the user asked for.
	// Also generates a histogram CSV if the user wanted it.
	std::cout << "Saving: " << filePath << std::endl;
	saveFormat == true ? fileOperations.saveAscii(newCloud, filePath) : fileOperations.saveBinary(newCloud, filePath);
	if(createCSV == true && !newCloud->empty()) canopyClass.canopyRoutine(newCloud,filePath);
	return 0;
}
// ---------------------------------------------------------------------------------