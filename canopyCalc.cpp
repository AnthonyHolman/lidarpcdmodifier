/* ==================================================================================
   Filename: canopyCalc.cpp
   Author: Anthony Holman
   Date Created: July 15, 2016
   Description: Creates a histogram of plant canopy heights. Will probably include widths
                in the future. See header for general information on each function.
   ==================================================================================*/

#include "include/canopyCalc.hpp"


int CanopyCalculations::canopyRoutine(){
	// Local variables
	pcl::PointCloud<pcl::PointXYZ>::Ptr cloudCopy = pcl::PointCloud<pcl::PointXYZ>::Ptr (new pcl::PointCloud<pcl::PointXYZ>);
	int32_t start = 0; // Beginning of vector.
	int32_t end = fileOperations.getCloudPtr()->size() - 1; // End of vector.
	int16_t position = 0; // Used to find a '.' in a string to change file extension.
	std::string filename = fileOperations.getFilename(); // Path of the loaded file.
	std::string inputHolder = "_"; // Holds the user's input for safety.
	std::string CSVFilename = "_"; // The final name of the CSV file.
	char userInput = ' '; // Takes the first char from the user's input.

	// Checking if a file is loaded.
	if(fileOperations.getCloudPtr()->empty()){
		PCL_ERROR("Empty file. Returning to the menu...\n");
		return -1;
	}

	// Creating a copy of the loaded file's cloud so the order of points doesn't change.
	*cloudCopy = *fileOperations.getCloudPtr();


	// Menu that asks if the user wants to quickly save a CSV with the same name in the same directory
	// as the loaded file, or enter in their own full path for the csv file.
	std::cout << "\n\nThis function will create a CSV file that holds histogram data for\n"
			  << "plant canopy height.\n"
			  << "(1) Save the CSV file with the same name and in the same directory as the loaded file.\n"
			  << "(2) Enter a new name and directory for the CSV file.\n"
			  << "(q) Return to the main menu.\n";
	while(userInput == ' '){
		std::cout << "Input: ";
		getline(std::cin,inputHolder);
		userInput = inputHolder[0];

		switch(userInput){
			case '1':
				position = filename.find_last_of("."); // Checking if there is a file extension at the end.
				CSVFilename = (position > filename.length() - 6 ? 
					filename.substr(0,position) + "_histo.csv" : filename + "_histo.csv"); 
				break;
			case '2':
				std::cout << "Enter the full path (directory and name) of the CSV file.\n";
				std::cout << "Name: ";
				getline(std::cin,filename);
				position = filename.find_last_of("."); // Checking if there is a file extension at the end.
				CSVFilename = (position > filename.length() - 6 ? filename.substr(0,position) + ".csv" : filename + ".csv"); 
				break;
			case 'q':
				std::cout << "Returning to menu";
				return -1;
				break;
			default:
				std::cout << "Invalid Input\n";
				userInput = ' ';
				break;
		}
	}

	std::cout << "Quicksorting loaded file. This might take a while...\n";
	quickSortHeights(cloudCopy, start, end);

	std::cout << "Placing data in bins...\n";
	binHeights(cloudCopy, CSVFilename);

	return 0;
}

int CanopyCalculations::canopyRoutine(pcl::PointCloud<pcl::PointXYZ>::Ptr cloud, std::string filename){
	int32_t start = 0; // Beginning of vector.
	int32_t end = cloud->size() - 1; // End of vector.
	std::string CSVFilename = "_"; // Final name of the CSV file. 
	int16_t position = 0; // Used to check for a '.' to change the file extension.

	// Checking if the provided pointcloud is empty.
	if(cloud->empty()){
		PCL_ERROR("Empty file.\n");
		return -1;
	}

	// Changing the file extension to .csv
	position = filename.find_last_of(".");
	CSVFilename = (position > filename.length() - 6 ? filename.substr(0,position) + ".csv" : filename + ".csv");

	quickSortHeights(cloud, start, end); // Begin quick sort.
	std::cout << "\nSaving: " << CSVFilename << std::endl;
	binHeights(cloud,CSVFilename); // Bin heights and save .csv file.

	return 0;
}



int CanopyCalculations::quickSortHeights(pcl::PointCloud<pcl::PointXYZ>::Ptr cloud, const int32_t start, const int32_t end){
	// Ends branch of recursion if there are no more points to sort.
	if(start >= end){
		return 0;
	}

	// Grabs the pivot point so the boundaries change when the function is called recursivly.
	float partition = quickSortPartitioner(cloud, start, end);

	quickSortHeights(cloud, start, partition - 1);
	quickSortHeights(cloud, partition + 1, end);

	return 0;
}

int CanopyCalculations::quickSortPartitioner(pcl::PointCloud<pcl::PointXYZ>::Ptr cloud, const int32_t start, const int32_t end){
	// Local variables.
	const float mid = start + (end - start) / 2; // Finds center of current range in the vector.
	const float pivot = cloud->points[mid].z; // Sets the value in the center to be the pivot.
	int32_t leftTrack = start; // Current position on the left side of the range.
	int32_t rightTrack = end; // Current position on the right side of the range.

	// Swaps the pivot with the point at the start then incrementing the start position by 1.
	std::swap(cloud->at(mid),cloud->at(start));
	leftTrack += 1;

	// Check if the value at the left track is less than the pivot value. If so, increment the left track by 1.
	// Then check if the value at the right track is greater than the pivot value. If so, deincrement the right track by 1.
	// Then swap the two values if the left track hasn't passed the right track.
	while(leftTrack <= rightTrack){
		while(leftTrack <= rightTrack && cloud->points[leftTrack].z <= pivot){
			leftTrack++;
		}
		while(leftTrack <= rightTrack && cloud->points[rightTrack].z > pivot){
			rightTrack--;
		}
		if(leftTrack < rightTrack)
			std::swap(cloud->at(leftTrack), cloud->at(rightTrack));
	}

	// Move the pivot value left of the left track, which should be in it's correct position.
	std::swap(cloud->at(start), cloud->at(leftTrack - 1));

	// Return the position of the pivot value.
	return leftTrack - 1;
}

int CanopyCalculations::binHeights(pcl::PointCloud<pcl::PointXYZ>::Ptr cloud, std::string filename){
	float increment = 0; // The size of each bin.
	float binRangeLeft = 0; // The left bound of the current bin.
	float binRangeRight = 0; // The right bound of the current bin.
	int32_t binSize = std::sqrt(cloud->size() + 1); // The total amount of bins.
	int32_t pointIndex = 0; // The current position in the pointcloud.
	int32_t cumulativeTotal = 0; // The total amount of points at the current position of the bin.
	std::ofstream histogramCSV; // The CSV file that will be saved.

	// Setting the boundaries of the bins.
	if(binSize > 50) binSize = 50; // Over 50 bins is too many, change it to 50 if it's over.
	increment = (cloud->back().z - cloud->front().z) / binSize;
	binRangeLeft = cloud->front().z;
	binRangeRight = cloud->front().z + increment;

	// Opening the file and prevent scientific notation as file is being written to.
	histogramCSV.open(filename);
	histogramCSV << std::fixed;

	// If the directory is invalid, exit.
	if(!histogramCSV.is_open()){
		std::cout << "File was unable to be created. Returning to the menu.\n";
		return -1;
	}

	// Header of the CSV.
	histogramCSV << "Height,Number of Points,Cumulative Points\n";


	for(int32_t binIndex = 0; binIndex < binSize; binIndex++){
		bin.push_back(std::vector<float>()); // Create new bin at the end.

		while(    cloud->points[pointIndex].z >= binRangeLeft 
			   && cloud->points[pointIndex].z < binRangeRight
			   && pointIndex < cloud->size()){

			bin.back().push_back(cloud->points[pointIndex].z); // Add point to the bin if it fits requirements.
			pointIndex += 1;
		}

		cumulativeTotal += bin.at(binIndex).size(); // Update current point total.

		// Write bin position, total points in bin, and the current point total to file.
		histogramCSV << removeTrailingZeros(binRangeLeft) << "," 
					 << bin.at(binIndex).size() << ","
					 << cumulativeTotal << "\n";

		// Moving bounds to fit the next bin.
		binRangeLeft += increment;
		binRangeRight += increment;
	}

	histogramCSV.close();

	return 0;
}

inline void CanopyCalculations::emptyBin(){
	bin.clear();
}

inline std::string CanopyCalculations::removeTrailingZeros(float inputFloat){
	// Local variables
	std::string outputString;

	// Converting float to string.
	outputString = boost::lexical_cast<std::string>(inputFloat);

	// Removing trailing 0's.
	if(outputString.find(".") != -1){
		while(outputString.back() == '0'){
			outputString.erase(outputString.length() - 1);
		}
	}		

	return outputString;
}