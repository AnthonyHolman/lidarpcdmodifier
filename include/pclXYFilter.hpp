/* ==================================================================================
   Filename: pclXYFilter.hpp
   Author: Anthony Holman
   Date Created: April 28, 2016
   Description: Filters out a pointcloud's points outside a specified container.
   ==================================================================================*/
#pragma once
/* ---------------------------------------------------------------------------------
   Include files
   ---------------------------------------------------------------------------------*/
#include <iostream>
#include <fstream>
#include <limits>

#include "canopyCalc.hpp"

#include <pcl/point_types.h>
#include <pcl/filters/crop_hull.h>
#include <pcl/Vertices.h>

#include <boost/algorithm/string/trim.hpp>
// ---------------------------------------------------------------------------------

/* ---------------------------------------------------------------------------------
   Class Prototype: XYFilter
   ---------------------------------------------------------------------------------*/
class XYFilter{
	private:
		// Instance variables.
		pcl::PointCloud<pcl::PointXYZ>::Ptr hull; // Holds the points that make up the outer shell 
												  // used for filtering points outside of the shell. 
		pcl::Vertices indices; // Tells crop hull which points in the shell make up the hull. Even
		 					   // though all the points in the hull are required, the function needs it.
		 					   // These do not contain actual points!
		bool createCSV; // If true, it uses the class within canopyCalc.hpp to generate a histogram of heights.
		bool cropHullToggle; // If true, it uses the crop hull pointcloud functions.
		                     // If false it uses a very basic conditional statement to filter the points.

		// Functions.
		/**
		 * Explicitly for manual point entry for filtering.
		 * @return -1 if it fails, 0 if it succeeds. For error checking.
		 */
		int setXYValues();

		/**
		 * Used for splitting up a single large PCD file into many smaller ones 
		 * based on a text file that contains the verticies for multiple containers.
		 * The function asks the user for the directory and name of the information file.
		 * Then it asks various questions to determine how the user wants to save the files
		 * and if they want to generate histogram data along with it
		 * @return -1 if it fails, 0 if it succeeds. For error checking.
		 */
		int prepareBatch();

		/**
		 * The loaded pointcloud generated in pclIO.hpp has it's points filtered here using the pointcloud crop hull
		 * functions. This does modify the loaded file and it needs to be saved before any changes are finalized. 
		 * @return -1 if it fails, 0 if it succeeds. For error checking.
		 */
		int removeOutliersCropHull();
		
		/**
		 * The loaded pointcloud generated in pclIO.hpp has it's points filtered here using the pointcloud crop hull
		 * functions. This does NOT modify the loaded file. It filters the points into a seperate pointcloud and then
		 * passes the cloud to the save function in pclIO.hpp and saves it in the location specified in prepareBatch.
		 * @param filePath    Holds the user's inputted directory and filename.
		 * @param saveFormat  If true, the file is saved in ascii. If false it's saved in binary.
		 * @param canopyClass Class instance used for creating histograms if the option is enabled.
		 * @return            -1 if it fails, 0 if it succeeds. For error checking.
		 */
		int removeOutliersCropHull(std::string &filePath, bool &saveFormat, CanopyCalculations &canopyClass);
		
		/**
		 * The loaded pointcloud generated in pclIO.hpp has it's points filtered here using simple conditional
		 * statements. The functionality of this filter is very limited as it will only filter out points within
		 * a square box with no skew. This is likely temporary and will possibly be removed as soon as crop hull
		 * is deemed to be completely reliable. This does modify the loaded file and it needs to be saved 
		 * before any changes are finalized. 
		 * @return -1 if it fails, 0 if it succeeds. For error checking.
		 */
		int removeOutliers();
		
		/**
		 * The loaded pointcloud generated in pclIO.hpp has it's points filtered here using simple conditional
		 * statements. The functionality of this filter is very limited as it will only filter out points within
		 * a square box with no skew. This is likely temporary and will possibly be removed as soon as crop hull
		 * is deemed to be completely reliable. This does NOT modify the loaded file. It filters the points into a 
		 * seperate pointcloud and then passes the cloud to the save function in pclIO.hpp and saves it in the 
		 * location specified in prepareBatch.
		 * @param filePath    Holds the user's inputted directory and filename.
		 * @param saveFormat  If true, the file is saved in ascii. If false it's saved in binary.
		 * @param canopyClass Class instance used for creating histograms if the option is enabled.
		 * @return            -1 if it fails, 0 if it succeeds. For error checking.
		 */
		int removeOutliers(std::string &filePath, bool &saveFormat, CanopyCalculations &canopyClass);
		
		/**
		 * Used to return a substring of a string using a delimiter.
		 * @param  fileLine The line from a text file containing verticies.
		 * @return          Returns an x or y value from fileLine in string form. 
		 */
		std::string grabToken(std::string &fileLine);

	public:
		// Functions
		/**
		 * Used purely to initialize the variables in this header file.
		 */
		XYFilter();

		/**
		 * Checks if a file is loaded, if the user wants to filter a single PCD or in batch, 
		 * and if they want to filter with crop hull or not.
		 * @return -1 if it fails, 0 if it succeeds. For error checking.
		 */
		int filterRoutine();
};
// ---------------------------------------------------------------------------------