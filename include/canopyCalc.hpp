/* ==================================================================================
   Filename: canopyCalc.hpp
   Author: Anthony Holman
   Date Created: July 15, 2016
   Description: Creates a histogram of plant canopy heights. Will probably include widths
                in the future.
   ==================================================================================*/
#pragma once

#include <cmath>

#include <pcl/impl/point_types.hpp>

class CanopyCalculations{
	private:
		// Instance variables.
		std::vector<std::vector<float>> bin; // 2D vector of floats. Rows hold the bins, columns hold the points.

		/**
		 * An implementation of a quicksort algorithm that sorts the heights of plants in a pointcloud from least to greatest.
		 * Calls itself recursively until the job is done.
		 * @param  cloud A point cloud that needs to be sorted.
		 * @param  start The left part of the range of the pointcloud where the sorting is taking place. 
		 * @param  end   The right part of the range of the pointcloud where the sorting is taking place.
		 * @return       -1 if it fails, 0 if it succeeds. For error checking.
		 */
		int quickSortHeights(pcl::PointCloud<pcl::PointXYZ>::Ptr cloud, const int32_t start, const int32_t end);

		/**
		 * The actual sorting portion of the algorithm. Sorts numbers around a pivot point in the center.
		 * @param  cloud A point cloud that needs to be sorted.
		 * @param  start The left part of the range of the pointcloud where the sorting is taking place. 
		 * @param  end   The right part of the range of the pointcloud where the sorting is taking place.
		 * @return       The position of the index where the pivot was.
		 */
		int quickSortPartitioner(pcl::PointCloud<pcl::PointXYZ>::Ptr cloud, const int32_t start, const int32_t end);

		/**
		 * Looks at the entire range of quicksorted heights and divides that up into seperate bins up to 50. Any point that falls
		 * within the range of that bin gets placed in it. Also cumulativly adds up the total amount of points as they are placed
		 * into the bins. The bin range, the total points in each bin, and the cumulative total at each bin gets written to a 
		 * CSV file.
		 * @param  cloud    The quicksorted cloud that will have it's points separated into bins.
		 * @param  filename The full path of the CSV file that will be created.
		 * @return          -1 if it fails, 0 if it succeeds. For error checking.
		 */
		int binHeights(pcl::PointCloud<pcl::PointXYZ>::Ptr cloud, std::string filename);
	public:
		/**
		 * Prepares the variables needed to bin the heights (z coordinates) into different containers and to generate a CSV file
		 * for a histogram. This function is called from the main menu and is used to create a single CSV file from one 
		 * pointcloud. Asks the user to input a filename for the CSV.
		 * @return -1 if it fails, 0 if it succeeds. For error checking.
		 */
		int canopyRoutine();

		/**
		 * Prepares the variables needed to bin the heights (z coordinates) into different containers and to generate a CSV file
		 * for a histogram. Asks no questions as the filename and the input pointcloud is already given.
		 * @param  cloud    The pointcloud that will be used to generate the CSV.
		 * @param  filename What the CSV file will be named.
		 * @return          -1 if it fails, 0 if it succeeds. For error checking.
		 */
		int canopyRoutine(pcl::PointCloud<pcl::PointXYZ>::Ptr cloud, std::string filename);

		/**
		 * Simply empties the bin vector so it can be reused for a different pointcloud.
		 */
		inline void emptyBin();

		/**
		 * Takes a float, converts it into a string, then removes the trailing zeros.
		 * @param  inputFloat The float that will be converted to a string.
		 * @return            The float in string form minus the trailing zeroes.
		 */
		inline std::string removeTrailingZeros(float inputFloat);
};

