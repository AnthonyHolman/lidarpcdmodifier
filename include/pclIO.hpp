/* ==================================================================================
   Filename: pclIO.hpp
   Author: Anthony Holman
   Date Created: April 22, 2016
   Description: Handles the loading and saving of PCL files.
   ==================================================================================*/
#pragma once
/* ---------------------------------------------------------------------------------
   Include files
   ---------------------------------------------------------------------------------*/
#include <iostream>

#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>
// ---------------------------------------------------------------------------------

/*---------------------------------------------------------------------------------
  Class: CloudIO
  ---------------------------------------------------------------------------------*/
class CloudIO{
	private:
		// Instance variables.
		std::string filename; // Holds the loaded file name
		pcl::PointCloud<pcl::PointXYZ>::Ptr cloudPtr; // Boost pointer that holds a new instance of a PointXYZ
													  // type. No need to delete, it's handled behind the scenes. 

	public:
		/**
		 * Constructor only initializes the variables.
		 */
		CloudIO();

		/**
		 * Asks the user to input the full path to a file and then loads it using the pointcloud
		 * library.
		 * @return -1 if it fails, 0 if it succeeds. For error checking.
		 */
		int loadFile(); 

		/**
		 * Overloaded function for loading a file at the command line as an argument.
		 * @param  char argv[1] from main.
		 * @return -1 if it fails, 0 if it succeeds. For error checking.
		 */
		int loadFile(char[]);

		/**
		 * Returns the name of the loaded file.
		 * @return filename
		 */
		std::string getFilename();

		/**
		 * Simply returns a pointer to a pointcloud XYZ data type (pcl::PointCloud<pcl::PointXYZ>).
		 * @return ptrCopy 
		 */
		pcl::PointCloud<pcl::PointXYZ>::Ptr getCloudPtr();

		/**
		 * Asks the user various questions for how they would like to save the loaded file.
		 * @return -1 if it fails, 0 if it succeeds. For error checking.
		 */
		int saveInstructions();

		/**
		 * Saves the PointXYZ loaded file into binary format. Use this if you need speed.
		 * @param  pcl::PointCloud<pcl::PointXYZ>::Ptr A pointer to a PointXYZ pointcloud.
		 * @param  string& Consists of two parts. The directory where the loaded file should be saved
		 *                 and the filename for what the filename will be saved as.
		 * @return -1 if it fails, 0 if it succeeds. For error checking.
		 */
		int saveBinary(pcl::PointCloud<pcl::PointXYZ>::Ptr, std::string&);

		/**
		 * Saves the PointXYZ loaded file into ascii format. Use this if you need readability.
		 * @param  pcl::PointCloud<pcl::PointXYZ>::Ptr* A pointer to a PointXYZ pointcloud.
		 * @param  string& Consists of two parts. The directory where the loaded file should be saved
		 *                 and the filename for what the filename will be saved as.
		 * @return -1 if it fails, 0 if it succeeds. For error checking.
		 */
		int saveAscii(pcl::PointCloud<pcl::PointXYZ>::Ptr, std::string&);
} fileOperations;
// ---------------------------------------------------------------------------------