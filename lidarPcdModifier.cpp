/* ==================================================================================
   Filename: lidarPclModifier.cpp
   Author: Anthony Holman
   Date Created: April 22, 2016
   Description: The .cpp file containing main. This program is used to perform various
                modifications on a single pcd file.
   ==================================================================================*/

/* ---------------------------------------------------------------------------------
   Include files
   ---------------------------------------------------------------------------------*/
#include <iostream>

#include "pclIO.cpp"
#include "pclXYFilter.cpp"
#include "canopyCalc.cpp"
// ---------------------------------------------------------------------------------

/* ---------------------------------------------------------------------------------
   Forward declarations
   ---------------------------------------------------------------------------------*/
/**
 * Main function. The program is currently set up to take only one, or no arguments.
 */
int main(int, char**);

/**
 * Outputs the filename and the list of operations for what the program can do.
 * Be sure to add to this and callSelection if you add functionality to the program.
 */
void printMenu();

/**
 * Runs the corresponding functions of the program depending on the user's input.
 * @param  char& The user's input taken in main.
 */
void callSelection(char&);

/**
 * Starts the filtering functions located in pclXYFilter.cpp and .hpp.
 */
int runFilter();

/**
 * Starts the canopy width and height calculation in canopyCalc.cpp and .hpp.
 */
int runCanopyCalc();
// ---------------------------------------------------------------------------------

/* ---------------------------------------------------------------------------------
   Functions
   ---------------------------------------------------------------------------------*/
int main(int argc, char *argv[]){
	std::string inputHolder = ""; // Used to avoid errors if the user enters more than one char.
	char menuSelection = ' ';

	// To avoid scientific notation.
	std::cout << std::fixed;

	// If filename is given in command line, automatically load the file.
	if(argc == 2){
		std::cout << "\nAutoloading file '" << argv[1] << "'...";
		fileOperations.loadFile(argv[1]);
	}

	// Brief introduction.
	std::cout << "\nLidar PCD Modifier\n"
	     << "Author: Anthony Holman\n"
	     << "This program implements various functions from the PointCloud library (pointclouds.org)\n"
	     << "in order to modify pointcloud files (.pcd) to better analyze data acquired from LIDAR.\n\n"
	     << "To begin, you must load a file. You can either do this from the menu or by inputting the\n"
	     << "file path as an argument as you begin the program. Read the readme to see details and examples\n"
	     << "for each function in the menu.";


	// Prints out the list of program functions, gets user input, and sends the user input to callSelection.
	while(menuSelection != 'q'){
		std::cout << "\n\n\n\n\n";
		printMenu();
		std::cout << "Selection: ";
		getline(std::cin,inputHolder);
		menuSelection = inputHolder.at(0);
		callSelection(menuSelection);
	}
	return 0;
}

void printMenu(){
	std::cout << "-----------------------------\n"
		 << "Loaded file: " << fileOperations.getFilename()
		 << "\n-----------------------------\n"
		 << "Options:\n"
		 << "(1) Load a new file\n"
		 << "(2) Save the file\n"
		 << "(3) Remove all points within a specified 2D container\n"
		 << "(4) Create plant height histogram\n"
		 << "(q) Quit the program\n";
}

// Calls the functions of the program specified in the switch statement.
void callSelection(char &menuSelection){

	switch(menuSelection){
		case '1' : // Uses the CloudIO class from pclIO.hpp to perform load functions.
			fileOperations.loadFile();
			break;
		case '2' : // Uses the CloudIO class from pclIO.hpp to perform save functions.
			std::cout << (fileOperations.saveInstructions() == -1 ? 
				"\nSave Failed/Aborted" :
				"\nSave Sucessful");
			break;
		case '3' : // Uses the XYFilter class from pclXYFilter.hpp to perform filter functions.
			std::cout << (runFilter() == -1 ? "\nFiltering Failed/Aborted" : "\nPoints Filtered");
			break;
		case '4' : // Nothing yet!
		    std::cout << (runCanopyCalc() == -1 ? "\nFunction Failed/Aborted" : "\nFunction Sucessful");
		    break;
		case 'q' : // Breaks the while loop in main and exits the program.
			std::cout << "Exiting Program\n\n";
			break;
		default :  // Prevents the user from entering something they shouldn't.
			std::cout << "Error: Invalid selection!\n";
			break;
	}
}

int runFilter(){
	XYFilter removePoints;
	return removePoints.filterRoutine();
}

int runCanopyCalc(){
	CanopyCalculations beginProcess;
	return beginProcess.canopyRoutine();
}
// ---------------------------------------------------------------------------------