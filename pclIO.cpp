/* ==================================================================================
   Filename: pclIO.cpp
   Author: Anthony Holman
   Date Created: April 22, 2016
   Description: Handles the loading and saving of PCL files. See the header for general
                comments on each function.
   ==================================================================================*/

#pragma once
/* ---------------------------------------------------------------------------------
   Include files
   ---------------------------------------------------------------------------------*/
#include "include/pclIO.hpp"
// ---------------------------------------------------------------------------------

/* ---------------------------------------------------------------------------------
   Class: CloudIO
   ---------------------------------------------------------------------------------*/
CloudIO::CloudIO(){
	// Initializing variables mentioned in header.
	filename = "No File Loaded";
	cloudPtr = pcl::PointCloud<pcl::PointXYZ>::Ptr (new pcl::PointCloud<pcl::PointXYZ>);
}

int CloudIO::loadFile(){
	// Local variables.
	std::string inputHolder = " "; // Used to avoid errors if the user enters more than one char.
	char userInput = ' ';

	// Checking if a file is already loaded. Warns user and asks if they want to
	// load a new file on top of it. If yes, wipes the loaded file regardless
	// if the new load succeeds.
	if(!cloudPtr->empty()){
		std::cout << "WARNING: There is a file already loaded. Continuing will\n"
			 << "discard all changes made to the currently loaded file.\n";

		while(userInput == ' '){
			std::cout << "Continue? (Y/N): ";
			getline(std::cin,inputHolder);
			userInput = inputHolder[0];

			switch(userInput){	
				case 'Y':
				case 'y':
					cloudPtr->clear();
					break;
				case 'N':
				case 'n':
					std::cout << "Returning to main menu.";
					return -1;
					break;
				default:
					std::cout << "Invalid Input";
					userInput = ' ';
					break;
			}
		}
	}

	// Getting user input for the path to the file they wish to load. Storing in filename.
	std::cout << "Enter the full path (directory and name) of the file you are loading.\n\n";
	std::cout << "Input: ";
	getline(std::cin,filename);

	// Calls the loadPCDFile function located in pclIO.hpp. Loads PCD file into memory.
	if(pcl::io::loadPCDFile<pcl::PointXYZ>(filename, *cloudPtr) == -1){
		PCL_ERROR("Couldn't read file");
		filename = "No File Loaded";
		return -1;
	}

	// Displays the first set of points in the PCD file so the user can see if the file loaded successfully.
	std::cout << "\nTesting, printing out the first row of points...\n"
		<< "X: " << cloudPtr->points[0].x << std::endl
		<< "Y: " << cloudPtr->points[0].y << std::endl
		<< "Z: " << cloudPtr->points[0].z << std::endl;
	return 0;
}


int CloudIO::loadFile(char cmdFileName[]){
	// Converting the c style string to a c++ style string.
	filename = std::string(cmdFileName);

	// Calls the loadPCDFile function located in pclIO.hpp. Loads PCD file into memory.
	if(pcl::io::loadPCDFile<pcl::PointXYZ>(filename, *cloudPtr) == -1){
		PCL_ERROR("Couldn't read file");
		filename = "No File Loaded";
		return -1;
	} 

	// Displays the first set of points in the PCD file so the user can see if the file loaded successfully.
	std::cout << "\nTesting...\n"
		<< cloudPtr->points[0].x << std::endl
		<< cloudPtr->points[0].y << std::endl
		<< cloudPtr->points[0].z;
	return 0;
}

std::string CloudIO::getFilename(){
	return filename;
}

pcl::PointCloud<pcl::PointXYZ>::Ptr CloudIO::getCloudPtr(){
	return cloudPtr;
}

int CloudIO::saveInstructions(){
	std::string inputHolder = " ";  // Used to avoid errors if the user enters more than one char.
	std::string filePath = " "; // The path for where the user wants to save the file and what they want to call it.
	char userInput = ' ';

	// Checks if there are any points stored in the loaded file. Stored in a vector in the PointXYZ data type.
	if(cloudPtr->empty()){
		PCL_ERROR("Empty file. Returning to the menu...\n");
		return -1;
	}

	// Asking the user for the format they would like to save the loaded file in. Passes the loaded file and
	// filePath into the corresponding save function. (ascii or binary)
	std::cout << "\n\n(1) Save in binary\n"
		<< "(2) Save in ascii\n"
		<< "(q) Abort save\n"
		<< "Selection: ";
	while(userInput == ' '){
		std::cout << "Input: ";
		getline(std::cin,inputHolder);
		userInput = inputHolder[0];

		switch(userInput){
			case '1':
				std::cout << "Enter the full path (directory and name) of the file you are saving.\n"
					 << "Input: ";
				getline(std::cin,filePath);
				return (saveBinary(cloudPtr, filePath) == -1) ? -1 : 0;
				break;
			case '2':
				std::cout << "Enter the full path (directory and name) of the file you are saving.\n"
					 << "Input: ";
				getline(std::cin,filePath);
				return (saveAscii(cloudPtr, filePath) == -1) ? -1 : 0;
				break;
			case 'q':
				std::cout << "Returning to menu\n";
				return -1;
				break;
			default:
				std::cout << "Invalid Input\n";
				userInput = ' ';
				break;
		}
	}
}

int CloudIO::saveBinary(pcl::PointCloud<pcl::PointXYZ>::Ptr saveFile, std::string &path){

	// Returns a fail if the file is empty.
	if(saveFile->size() == 0){
		PCL_ERROR("File empty!\n");
		return -1;
	}

	// Calls the save function with an argument to save the file in binary. Uses the library's 
	// exception handling to throw an error if the save fails.
	try{
    	if(pcl::io::savePCDFile(path, *saveFile, true) == -1)
			PCL_THROW_EXCEPTION(pcl::IOException, "Invalid input! Incorrect path?");
		return 0;
	}
	catch(pcl::IOException& e){
		PCL_ERROR("Invalid input! Incorrect path?\n");
		return -1;
	}
}

int CloudIO::saveAscii(pcl::PointCloud<pcl::PointXYZ>::Ptr saveFile, std::string &path){
	// Returns a fail if the file is empty.
	if(saveFile->size() == 0){
		PCL_ERROR("File empty!\n");
		return -1;
	}

	// Calls the save function with an argument to save the file in ascii. Uses the library's 
	// exception handling to throw an error if the save fails.
	try{
    	if(pcl::io::savePCDFile(path, *saveFile, false) == -1)
			PCL_THROW_EXCEPTION(pcl::IOException, "Invalid input! Incorrect path?");
		return 0;
	}
	catch(pcl::IOException& e){
		PCL_ERROR("Invalid input! Incorrect path?\n");
		return -1;
	}
}


// ---------------------------------------------------------------------------------